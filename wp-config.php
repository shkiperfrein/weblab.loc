<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', 'wordpress');

/** MySQL hostname */
define('DB_HOST', 'mysql:3306');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', '');


/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'PB+7rW%)ZUQi(mR=dWCe~mI3qXes4fbu^b!|0QEH !bW9&;2z:Bggzjbz(7<yUu6');
define('SECURE_AUTH_KEY',  'Q=^`(J(zjLrUEXT>NO}~](`kh8-1FTp}Ul_/.IP?F3dMUxo t#D}.=7)soI;)5G>');
define('LOGGED_IN_KEY',    '82q.xscgF$(6IX/LkZ&rHqvoj5o-#nN=`ntem0-1QMz&s|yRH}FGo~g*>_]|,7a<');
define('NONCE_KEY',        'M[Th[d(B.^ieQnJ[Z-8+lkm7mW^g$+4@0Kl)Mv.=+xb9=CCou2CR(G}zUl3+5=fL');
define('AUTH_SALT',        'JaWNo35#PDP|nQRcC8fu{91B`rM$16lS=Oznsxo9}+C2cgh6lvgDA8}9xo2YLga6');
define('SECURE_AUTH_SALT', '+j0Nu&P4-d (C>pphFmx^NnaugCH=TtWwUYQDX~n5Wh:qs~|L/Nk?.xt?c{NCau_');
define('LOGGED_IN_SALT',   'ApS(,J@`^Zq}*sPHRkxZ%{z{?S.o66ZugeJoV&iv4z+Yms NcC>~PFZrlsgg}m.v');
define('NONCE_SALT',       'VSS-,];OT7FK-}r5e9|./3[w@/qe{.R,dY]`GCAlCZxH,C9[&*z=7p#8v,~4kK},');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('FS_METHOD', 'direct');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
